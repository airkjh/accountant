package net.ion.kpf.accountant;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ServiceRange {

    private static DateTimeFormatter yyyyMM = DateTimeFormatter.ofPattern("yyyyMM");

    private final LocalDate start;
    private final LocalDate end;
    private List<String> ranges;

    ServiceRange(LocalDate start, LocalDate end) {
        this.start = start;
        this.end = end;
    }

    List<String> rangeYearMonth() {
        if (ranges != null) return ranges;

        long range = start.until(end, ChronoUnit.MONTHS);
        this.ranges = Stream
                .iterate(start, start -> start.plusMonths(1))
                .limit(range)
                .map(ld -> ld.format(yyyyMM))
                .collect(Collectors.toList());

        return ranges;
    }

    public boolean contains(YearMonth yearMonth) {
        return ranges.contains(yearMonth.value());
    }

}
