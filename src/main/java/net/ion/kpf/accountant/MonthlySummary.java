package net.ion.kpf.accountant;

import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MonthlySummary {

    private final List<License> licenses = Lists.newArrayList();
    private final Map<String, BigDecimal> newLicenseSummary;
    private final Map<String, BigDecimal> renewedLicenseSummary;

    public MonthlySummary(License... licenses) {
        this.licenses.addAll(Arrays.asList(licenses));

        this.newLicenseSummary = calcNewLicense();
        this.renewedLicenseSummary = calcRenewedLicense();
    }

    private Map<String, BigDecimal> calcNewLicense() {
        return calculate(License.ContractType.newly);
    }

    private Map<String, BigDecimal> calcRenewedLicense() {
        return calculate(License.ContractType.renewed);
    }

    private Map<String, BigDecimal> calculate(final License.ContractType type) {
        return licenses.stream()
                .filter(license -> type == license.type())
                .map(LicenseMonthlySales::new)
                .map(LicenseMonthlySales::monthlyStats)
                .flatMap(monthlyStats -> monthlyStats.entrySet().stream())
                .collect(Collectors.groupingBy(
                        Map.Entry::getKey,
                        Collectors.reducing(BigDecimal.ZERO, Map.Entry::getValue, BigDecimal::add))
                );
    }

    public BigDecimal newLicenseSales(YearMonth yearMonth) {
        return newLicenseSummary.getOrDefault(yearMonth.value(), BigDecimal.ZERO);
    }

    public BigDecimal renewedLicenseSales(YearMonth yearMonth) {
        return renewedLicenseSummary.getOrDefault(yearMonth.value(), BigDecimal.ZERO);
    }

    public BigDecimal totalMonthSales(YearMonth yearMonth) {
        return newLicenseSales(yearMonth).add(renewedLicenseSales(yearMonth));
    }

}
