package net.ion.kpf.accountant;

import java.math.BigDecimal;
import java.time.LocalDate;

public class LicenseBuilder {

    private String licenseId;

    private LocalDate contractDate = LocalDate.now();
    private LocalDate serviceStartDate = LocalDate.now();
    private LocalDate serviceEndDate = serviceStartDate.plusYears(1);
    private BigDecimal price;
    private License.ContractType type = License.ContractType.newly;

    public static LicenseBuilder builder(String licenseId) {
        return new LicenseBuilder(licenseId);
    }

    public LicenseBuilder(String licenseId) {
        this.licenseId = licenseId;
    }

    public LicenseBuilder contractMadeAt(LocalDate contractDate) {
        this.contractDate = contractDate;
        return this;
    }

    public LicenseBuilder serviceStartAt(LocalDate serviceStartDate) {
        this.serviceStartDate = serviceStartDate;
        return this;
    }

    public LicenseBuilder serviceEndAt(LocalDate serviceEndDate) {
        this.serviceEndDate = serviceEndDate;
        return this;
    }

    public LicenseBuilder withPrice(long price) {
        this.price = BigDecimal.valueOf(price);
        return this;
    }

    public LicenseBuilder newLicense() {
        this.type = License.ContractType.newly;
        return this;
    }

    public LicenseBuilder renewedLicense() {
        this.type = License.ContractType.renewed;
        return this;
    }

    public License build() {

        if (invalidPrice()) {
            throw new IllegalStateException("Invalid price value : null zero or negative value");
        }

        License license = new License();
        license.licenseId(licenseId);
        license.contractDate(contractDate);
        license.contractPrice(price);
        license.type(type);
        license.serviceStartDate(serviceStartDate);
        license.serviceEndDate(serviceEndDate);

        return license;
    }

    private boolean invalidPrice() {
        return price == null || BigDecimal.ZERO.compareTo(price) >= 0;
    }


}
