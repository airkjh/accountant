package net.ion.kpf.accountant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDate;

@Accessors(fluent = true)
@Setter(AccessLevel.PACKAGE)
@Getter
public class License {

    enum ContractType {
        newly, renewed
    }

    private String licenseId;
    private LocalDate contractDate;
    private LocalDate serviceStartDate;
    private LocalDate serviceEndDate;
    private BigDecimal contractPrice;
    private ContractType type;

    License() {
    }

}
