package net.ion.kpf.accountant;

import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Getter
public class YearMonth {

    private final int year;
    private final int month;

    private YearMonth(int year, int month) {
        this.year = year;
        this.month = month;
    }

    public static YearMonth of(int year, int month) {
        return new YearMonth(year, month);
    }

    public String value() {
        return String.format("%1s%2$02d", year, month);
    }

    @Override
    public String toString() {
        return value();
    }


}
