package net.ion.kpf.accountant;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class LicenseMonthlySales {

    private final License license;
    private final ServiceRange range;
    private final Map<String, BigDecimal> monthlyStats = new TreeMap<>();

    public LicenseMonthlySales(License license) {
        this.license = license;
        this.range = new ServiceRange(license.serviceStartDate(), license.serviceEndDate());

        calculate();
    }

    private void calculate() {
        List<String> months = range.rangeYearMonth();
        BigDecimal divider = new BigDecimal(months.size());
        BigDecimal aggregate = new BigDecimal(0);

        // calculate before last month
        for (int i = 0; i < months.size() - 1; i++) {
            BigDecimal monthlySales = license.contractPrice().divide(divider, RoundingMode.DOWN);
            aggregate = aggregate.add(monthlySales);
            monthlyStats.put(months.get(i), monthlySales);
        }

        // calculate last month
        String lastServiceMonth = months.get(months.size() - 1);
        BigDecimal lastServiceMonthSales = license.contractPrice().subtract(aggregate);
        monthlyStats.put(lastServiceMonth, lastServiceMonthSales);
    }

    public BigDecimal getSales(int year, int month) {
        return getSales(YearMonth.of(year, month));
    }

    public BigDecimal getSales(YearMonth yearMonth) {
        return monthlyStats.getOrDefault(yearMonth.value(), BigDecimal.ZERO);
    }

    public Map<String, BigDecimal> monthlyStats() {
        return monthlyStats;
    }

    public void debugPrint() {
        debugPrint(System.out);
    }

    public void debugPrint(PrintStream out) {
        monthlyStats.entrySet().stream()
                .map(entry -> String.format("%s ==> %s", entry.getKey(), entry.getValue()))
                .forEach(out::println);
    }
}
