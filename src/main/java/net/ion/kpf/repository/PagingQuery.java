package net.ion.kpf.repository;


import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Getter
public abstract class PagingQuery {

    protected int offset = 0;
    protected int limit = 15;

    protected PagingQuery() {}

    protected PagingQuery(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

}
