package net.ion.kpf.repository.press;

import java.util.List;

public interface PressRepository {

    void save(Press press);

    List<Press> query(PressQuery query) ;

}
