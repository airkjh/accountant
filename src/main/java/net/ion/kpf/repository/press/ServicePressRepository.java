package net.ion.kpf.repository.press;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

@Repository
@Qualifier("pressRepository")
@Profile("production")
public class ServicePressRepository implements PressRepository {

    @Override
    public void save(Press press) {
        throw new NotImplementedException();
    }

    @Override
    public List<Press> query(PressQuery query) {
        throw new NotImplementedException();
    }
}
