package net.ion.kpf.repository.press;

import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@Qualifier("pressRepository")
@Profile("test")
public class MapPressRepository implements PressRepository {

    private Map<String, Press> presses = Maps.newTreeMap();

    @Override
    public void save(Press press) {
        presses.put(press.id(), press);
    }

    @Override
    public List<Press> query(PressQuery query) {
        return presses.values().stream()
                .filter(press -> {
                    boolean match = true;

                    if (!StringUtils.isEmpty(query.id())) {
                        match = match && press.id().equals(query.id());
                    }

                    if (!StringUtils.isEmpty(query.name())) {
                        match = match && query.name().equals(press.name());
                    }

                    return match;
                })
                .skip(query.offset())
                .limit(query.limit())
                .collect(Collectors.toList());
    }
}
