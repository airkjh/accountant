package net.ion.kpf.repository.press;

import net.ion.kpf.repository.PagingQuery;

public class PressQuery extends PagingQuery {

    private String id;
    private String name;

    public PressQuery() {
        super();
    }

    public PressQuery(int offset, int limit) {
        super(offset, limit);
    }

    public static PressQuery all() {
        return new PressQuery(0, Integer.MAX_VALUE);
    }

    public PressQuery withId(String id) {
        this.id = id;
        return this;
    }

    public String id() {
        return id;
    }

    public PressQuery withName(String name) {
        this.name = name;
        return this;
    }

    public String name() {
        return name;
    }
}
