package net.ion.airkjh;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TestDumb {

    @Test
    public void bigDecimalCompareTo() {
        assertEquals(-1, BigDecimal.ZERO.compareTo(BigDecimal.valueOf(1)));
        assertEquals(0, BigDecimal.ZERO.compareTo(BigDecimal.valueOf(0)));
        assertEquals(1, BigDecimal.ZERO.compareTo(BigDecimal.valueOf(-1)));
    }

    @Test
    public void leftPad() {
        assertEquals("01", String.format("%1$02d", 1));
        assertEquals("10", String.format("%1$02d", 10));

        assertEquals("201610", String.format("%1s%2$02d", 2016, 10));
    }

}
