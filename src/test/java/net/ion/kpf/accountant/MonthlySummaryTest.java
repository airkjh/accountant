package net.ion.kpf.accountant;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class MonthlySummaryTest {

    @Test
    public void first() {
        LocalDate start = LocalDate.of(2017, 1, 1);
        License l1 = TestLicenseFactory.generate("test-license-1", start, start.plusYears(1), 10000000);
        License l2 = TestLicenseFactory.generate("test-license-2", start, start.plusMonths(3), 4000000);

        MonthlySummary summary = new MonthlySummary(l1, l2);
        LicenseMonthlySales l1MonthlySales = new LicenseMonthlySales(l1);
        LicenseMonthlySales l2MonthlySales = new LicenseMonthlySales(l2);

        BigDecimal l1Sales = l1MonthlySales.getSales(2017, 2);
        BigDecimal l2Sales = l2MonthlySales.getSales(2017, 2);
        assertEquals(l1Sales.add(l2Sales), summary.totalMonthSales(YearMonth.of(2017, 2)));

        l1Sales = l1MonthlySales.getSales(2017, 5);
        l2Sales = l2MonthlySales.getSales(2017, 5);
        assertEquals(BigDecimal.ZERO, l2Sales);
        assertEquals(l1Sales.add(l2Sales), summary.totalMonthSales(YearMonth.of(2017, 5)));

        assertEquals(BigDecimal.ZERO, summary.totalMonthSales(YearMonth.of(2018, 3)));
    }
}

