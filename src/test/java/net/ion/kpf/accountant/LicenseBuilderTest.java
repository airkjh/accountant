package net.ion.kpf.accountant;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class LicenseBuilderTest {

    @Test
    public void first() {
        LocalDate now = LocalDate.now();
        LocalDate start = LocalDate.of(2016, 1, 1);
        LocalDate end = start.plusYears(1);
        long price = 10000000;

        License license = LicenseBuilder.builder("test-license-1")
                .contractMadeAt(now)
                .serviceStartAt(start)
                .serviceEndAt(end)
                .withPrice(price)
                .build();

        assertEquals("test-license-1", license.licenseId());
        assertEquals(now, license.contractDate());
        assertEquals(start, license.serviceStartDate());
        assertEquals(end, license.serviceEndDate());
        assertEquals(price, license.contractPrice().longValue());
    }

    @Test
    public void defaultValue() {
        LocalDate now = LocalDate.now();
        LocalDate oneYearAfter = now.plusYears(1);

        License license = LicenseBuilder.builder("test-license-1").withPrice(100).build();
        LocalDate contractDate = license.contractDate();

        assertEquals(now.getYear(), contractDate.getYear());
        assertEquals(now.getMonth(), contractDate.getMonth());
        assertEquals(now.getDayOfMonth(), contractDate.getDayOfMonth());

        LocalDate startDate = license.serviceStartDate();
        assertEquals(now.getYear(), startDate.getYear());
        assertEquals(now.getMonth(), startDate.getMonth());
        assertEquals(now.getDayOfMonth(), startDate.getDayOfMonth());

        LocalDate endDate = license.serviceEndDate();
        assertEquals(oneYearAfter.getYear(), endDate.getYear());
        assertEquals(oneYearAfter.getMonth(), endDate.getMonth());
        assertEquals(oneYearAfter.getDayOfMonth(), endDate.getDayOfMonth());

    }

    @Test
    public void invalidPrice() {
        try {
            // price not specified
            LicenseBuilder.builder("test-license-1").build();
        } catch(IllegalStateException e) {}

        try {
            // price is zero value
            LicenseBuilder.builder("test-license-1").withPrice(0).build();
        } catch(IllegalStateException e) {}

        try {
            // price is negative value
            LicenseBuilder.builder("test-license-1").withPrice(-100).build();
        } catch(IllegalStateException e) {}
    }

}


