package net.ion.kpf.accountant;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class LicenseMonthlySalesTest {

    @Test
    public void first() {
        LocalDate start = LocalDate.of(2016, 10, 1);
        LocalDate end = start.plusYears(1);
        License license = TestLicenseFactory.generate("test-license-1", start, end, 10000000);

        LicenseMonthlySales monthlySales = new LicenseMonthlySales(license);

        Stream.iterate(start, month -> month.plusMonths(1)).limit(11)
//            .peek(yearMonth -> {
//                System.out.println(String.format("%s = %s", yearMonth, monthlySales.totalMonthSales(yearMonth.getYear(), yearMonth.getMonthValue())));
//            })
                .forEach(yearMonth -> {
                    BigDecimal sales = monthlySales.getSales(yearMonth.getYear(), yearMonth.getMonthValue());
                    assertEquals(833333, sales.intValue());
                });

        LocalDate lastYearMonth = end.minusMonths(1);
        assertEquals(833337, monthlySales.getSales(lastYearMonth.getYear(), lastYearMonth.getMonthValue()).intValue());
    }

    @Test
    public void shortRange() {
        License license = TestLicenseFactory.generate("test-license-1", LocalDate.of(2016, 10, 1), LocalDate.of(2017, 1, 1), 10000000);
        LicenseMonthlySales monthlySales = new LicenseMonthlySales(license);

//        monthlySales.debugPrint();
        assertEquals(3333333, monthlySales.getSales(2016, 10).intValue());
        assertEquals(3333333, monthlySales.getSales(2016, 11).intValue());
        assertEquals(3333334, monthlySales.getSales(2016, 12).intValue());
    }



}

