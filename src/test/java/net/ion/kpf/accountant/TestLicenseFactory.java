package net.ion.kpf.accountant;

import java.time.LocalDate;

public class TestLicenseFactory {

    public static License generate(String id, LocalDate start, LocalDate end, long price) {
        return LicenseBuilder.builder(id)
                .serviceStartAt(start)
                .serviceEndAt(end)
                .withPrice(price)
                .build();
    }

}
