package net.ion.kpf.repository.press;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class MapPressRepositoryTest {

    private MapPressRepository repo;

    @Before
    public void setUp() {
        this.repo = new MapPressRepository();
        fillTestRecords();
    }

    @Test
    public void query() {
        List<Press> allResult = repo.query(PressQuery.all());

        assertEquals(100, allResult.size());

        List<Press> filtered = repo.query(new PressQuery().withId("test-press-10"));
        assertEquals(1, filtered.size());
        assertEquals("test-press-10", filtered.get(0).id());
    }

    @Test
    public void paging() {
        List<Press> result1 = repo.query(new PressQuery(0, 10));

        assertEquals(10, result1.size());

        List<Press> result2 = repo.query(new PressQuery(95, 10));

        assertEquals(5, result2.size());

    }

    private void fillTestRecords() {
        for(int i = 0; i < 100; i++) {
            Press p = new Press(String.format("test-press-%s", i), String.format("press-%s", i));
            repo.save(p);
        }
    }

}